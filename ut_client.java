/**
 * ut_client.java
 *
 * Version : 1.0
 *
 * Revisions : 0
 */


/**
 * This program acts as a client side where the data is read from a
 * file and it is sent as packets to the server along with a checksum.
 * It waits for a ACK from the server and it sends the next packet 
 * when it receives it otherwise it retransmits the previous packet.
 */


import java.util.*;
import java.net.*;
import java.io.*;


 class header implements java.io.Serializable{
 	String text = new String();
 	int chksum;
 	int seq_num;
 }

class ut_client implements java.io.Serializable{

	static ArrayList<header> packets = new ArrayList<header>();

	public static void main(String args[])throws IOException, InterruptedException, ClassNotFoundException, FileNotFoundException{

		Scanner sc = new Scanner(System.in);
		System.out.println("Enter Self Port number");
		int port1 = sc.nextInt();
		System.out.println("Enter Other Port number");
		int port2 = sc.nextInt();
		InetAddress ip = InetAddress.getLocalHost();
		DatagramSocket sendingSocket = new DatagramSocket();
		DatagramSocket listeningSocket = new DatagramSocket(port1);
		byte[] outputArray = new byte[65535];
		byte[] inputArray = new byte[65535];
		int counter = 0;
		Scanner file_rd = new Scanner(new File("test.txt"));
		while(file_rd.hasNextLine()){
			header headerValue = new header();
			
			headerValue.text = file_rd.nextLine();
			headerValue.seq_num = counter;
			packets.add(headerValue);
			
			headerValue.chksum = headerValue.text.hashCode();
			System.out.println("Checksum outputArray is " + headerValue.chksum);

			ByteArrayOutputStream bos = new ByteArrayOutputStream();
			ObjectOutputStream out = new ObjectOutputStream(bos);
			out.writeObject(headerValue);
			out.close();
			outputArray = bos.toByteArray();
			DatagramPacket outputPacket = new DatagramPacket(outputArray,outputArray.length,ip,port2);
			DatagramPacket inputPacket = new DatagramPacket(inputArray,inputArray.length);
			bos.close();
			sendingSocket.send(outputPacket);
			try{
				listeningSocket.setSoTimeout(1000);
				listeningSocket.receive(inputPacket);
			}
			catch(SocketTimeoutException e){
				int c1 = 0;
				System.out.println("Time out");
				while(c1 == 0){
					sendingSocket.send(outputPacket);
					try{

						listeningSocket.setSoTimeout(1000);
						listeningSocket.receive(inputPacket);
						c1 = 1;
					}
					catch(SocketTimeoutException e1){
						System.out.println("Time out");
					}

				}
			}
			inputArray = inputPacket.getData();   			            
			ByteArrayInputStream bin = new ByteArrayInputStream(inputArray);
			ObjectInputStream in = new ObjectInputStream(bin);
			header recValue = (header) in.readObject();
			System.out.println(recValue.text);
			
			int cc = recValue.seq_num;
			if(recValue.seq_num != (counter + 1)){
				while(cc == counter){
					sendingSocket.send(outputPacket);
					inputArray = inputPacket.getData();   			            
					bin = new ByteArrayInputStream(inputArray);
					in = new ObjectInputStream(bin);
					recValue = (header) in.readObject();
					if(recValue.seq_num == (counter + 1))
						cc = counter + 1;
				}
			}

			counter++;
		}
	}
}