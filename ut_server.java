/**
 * ut_server.java
 *
 * Version : 1.0
 *
 * Revisions : 0
 */


/**
 * This program acts as a server for the TCP protocol to work
 * under the guise of UDP. The server side receives the packets
 * it first checks the checksum and if correct it writes the data
 * to the file. It also sets a timeout of 5 sec so as to disconnect
 * with the client
 */


import java.util.*;
import java.net.*;
import java.io.*;

class header implements java.io.Serializable{
 	String text = new String();
 	int chksum;
 	int seq_num;
 }

class ut_server implements java.io.Serializable{

	static ArrayList<header> packets = new ArrayList<header>();


	public static void main(String args[])throws IOException, InterruptedException, ClassNotFoundException{
		
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter Self Port number");
		int port1 = sc.nextInt();
		System.out.println("Enter Other Port number");
		int port2 = sc.nextInt();
		InetAddress ip = InetAddress.getLocalHost();
		DatagramSocket sendingSocket = new DatagramSocket();
		DatagramSocket listeningSocket = new DatagramSocket(port1);
		byte[] outputArray = new byte[65535];
		byte[] inputArray = new byte[65535];
		int counter = 0;
		FileWriter bw = new FileWriter("test_write.txt");
		
		try{
			listeningSocket.setSoTimeout(5000);

			while(true){
				header headerValue = new header();
				headerValue.text = new String("ACK ");
				headerValue.chksum = headerValue.text.hashCode();
				ByteArrayOutputStream bos = new ByteArrayOutputStream();
				ObjectOutputStream out = new ObjectOutputStream(bos);
				DatagramPacket inputPacket = new DatagramPacket(inputArray,inputArray.length);
			
				
				listeningSocket.receive(inputPacket);
				inputArray=inputPacket.getData();   			            
				ByteArrayInputStream bin = new ByteArrayInputStream(inputArray);
				ObjectInputStream in = new ObjectInputStream(bin);	
				header recValue = (header) in.readObject();
				packets.add(recValue);
				String recValueText = recValue.text;
				
				if(recValue.chksum == recValueText.hashCode()){
					headerValue.text += Integer.toString((recValue.seq_num+1));
					headerValue.seq_num = recValue.seq_num+1;
					out.writeObject(headerValue);
					out.close();
					outputArray = bos.toByteArray();
					DatagramPacket outputPacket = new DatagramPacket(outputArray,outputArray.length,ip,port2);
					sendingSocket.send(outputPacket);
					bos.close();
					String text_val = recValue.text+"\n";
					System.out.println("Packet number received " + recValue.seq_num);
					bw.write(text_val);
				
				}
				else{
					System.out.println("Checksum fail");
				}
				
			}
		}
		catch(Exception e){
			bw.close();
		}	            
	}
}